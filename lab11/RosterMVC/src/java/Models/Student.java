/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;

public class Student implements Serializable {
    private String LastName;   
    private String FirstName;  
    private String PsuId;
    private String Team;
    
    public Student() { }

    public void setLastName(String lastName) {
	this.LastName = lastName;
    }
    public String getLastName() {
	return this.LastName;
    }

    public void setFirstName(String firstName) {
	this.FirstName = firstName;
    }
    public String getFirstName() {
	return this.FirstName;
    }
    
    public void setPsuId(String psuId) {
	this.PsuId = psuId;
    }
    public String getPsuId() {
	return this.PsuId;
    }
    
    public void setTeam(String team) {
	this.Team = team;
    }
    public String getTeam() {
	return this.Team;
    }
}