/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.beans.XMLEncoder; // simple and effective
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

public class Roster {
    private static final String filename = "C:\\Users\\djh5533.PSU-ERIE.006\\Downloads\\cmpsc421-master\\cmpsc421-master\\lab09\\roster.db";
    private Map<String,Student> students;
  
    private ServletContext sctx;

    public Roster() { }

    // The ServletContext is required to read the data from
    // a text file packaged inside the WAR file
    public void setServletContext(ServletContext sctx) {
	this.sctx = sctx;
    }
    public ServletContext getServletContext() { return this.sctx; }

    // getPredictions returns an XML representation of
    // the Predictions array
    public void setPredictions(String ps) { } // no-op
    public String getPredictions() {
	// Has the ServletContext been set?
	if (getServletContext() == null) 
	    return null;      

	// Have the data been read already?
	if (students == null) 
	    populate(); 

	// Convert the Predictions array into an XML document
	return toXML();
    }

    //** utilities
    private void populate() {
	InputStream in;
        try {
            in = new FileInputStream(filename);
            // Read the data into the array of Predictions.
            try {
                InputStreamReader isr = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(isr);

                students = new HashMap<>();
                int i = 0;
                String record = null;
                while ((record = reader.readLine()) != null) {
                    String[] parts = record.split("!");
//                    System.out.println(parts.length);
                    Student s = new Student();
//                    System.out.println(parts[0] + " " + parts[1]);
                    if(parts.length==4){
                        s.setLastName(parts[0]);
                        s.setFirstName(parts[1]);
                        s.setPsuId(parts[2]);
                        s.setTeam(parts[3]);
                        students.put(s.getPsuId(), s);
                    }// if
                }// while
            } catch (IOException e) {
                System.out.println("IO EXCEPTION");
            }// try-catch
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        }// try-catch
    }
    
    public static void add(Student s){
        try {
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename,true)));
            out.write(String.format(
                    "%n%s!%s!%s!%s",
                    s.getLastName(),
                    s.getFirstName(),
                    s.getPsuId(),
                    s.getTeam()
            ));
            out.close();
        } catch (FileNotFoundException ex) {
            System.out.println("FILE NOT FOUND");
        } catch (IOException ex) {
            System.out.println("IO EXCEPTION");
        }
    }
    
    public static void delete (String id) throws IOException{
        InputStream in;
        Map<String,Student> students2 = null;
        try {
            in = new FileInputStream(filename);
            // Read the data into the array of Predictions.
            try {
                InputStreamReader isr = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(isr);

                students2 = new HashMap<>();
                int i = 0;
                String record = null;
                while ((record = reader.readLine()) != null) {
                    String[] parts = record.split("!");
//                    System.out.println(parts.length);
                    Student s = new Student();
//                    System.out.println(parts[0] + " " + parts[1]);
                    if(parts.length==4){
                        s.setLastName(parts[0]);
                        s.setFirstName(parts[1]);
                        s.setPsuId(parts[2]);
                        s.setTeam(parts[3]);
                        students2.put(s.getPsuId(), s);
                    }// if
                }
                students2.remove(id);
                
            } catch (IOException e) {
                System.out.println("IO EXCEPTION");
            }
            in.close();
            System.out.println("SIZE: "+students2.size());
            Collection c = students2.values();
            Iterator itr = c.iterator();
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename,false)));
            while(itr.hasNext()){
                Student s = (Student)itr.next();
                System.out.println(s.getFirstName());
                 
                try {
                  out.write(String.format(
                          "%s!%s!%s!%s%n",
                          s.getLastName(),
                          s.getFirstName(),
                          s.getPsuId(),
                          s.getTeam()
                  ));
                } catch (FileNotFoundException ex) {
                    System.out.println("FILE NOT FOUND");
                } catch (IOException ex) {
                    System.out.println("IO EXCEPTION");
                }
            }
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void update (Student newStudent) throws IOException{
        InputStream in;
        Map<String,Student> students2 = null;
        try {
            in = new FileInputStream(filename);
            // Read the data into the array of Predictions.
            try {
                InputStreamReader isr = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(isr);

                students2 = new HashMap<>();
                int i = 0;
                String record = null;
                while ((record = reader.readLine()) != null) {
                    String[] parts = record.split("!");
//                    System.out.println(parts.length);
                    Student s = new Student();
//                    System.out.println(parts[0] + " " + parts[1]);
                    if(parts.length==4){
                        s.setLastName(parts[0]);
                        s.setFirstName(parts[1]);
                        s.setPsuId(parts[2]);
                        s.setTeam(parts[3]);
                        students2.put(s.getPsuId(), s);
                    }
                }
                students2.remove(newStudent.getPsuId());
                students2.put(newStudent.getPsuId(), newStudent);
                
            } catch (IOException e) {
                System.out.println("IO EXCEPTION");
            }
            in.close();
            System.out.println("SIZE: "+students2.size());
            Collection c = students2.values();
            Iterator itr = c.iterator();
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename,false)));
            while(itr.hasNext()){
                Student s = (Student)itr.next();
                System.out.println(s.getFirstName());
                 
                try {
                  out.write(String.format(
                          "%s!%s!%s!%s%n",
                          s.getLastName(),
                          s.getFirstName(),
                          s.getPsuId(),
                          s.getTeam()
                  ));
                } catch (FileNotFoundException ex) {
                    System.out.println("FILE NOT FOUND");
                } catch (IOException ex) {
                    System.out.println("IO EXCEPTION");
                }
            }
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void deleteTeam(String team) throws IOException{
        InputStream in;
        Map<String,Student> students2 = null;
        try {
            in = new FileInputStream(filename);
            // Read the data into the array of Predictions.
            try {
                InputStreamReader isr = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(isr);

                students2 = new HashMap<>();
                int i = 0;
                String record = null;
                while ((record = reader.readLine()) != null) {
                    String[] parts = record.split("!");
//                    System.out.println(parts.length);
                    Student s = new Student();
//                    System.out.println(parts[0] + " " + parts[1]);
                    if(parts.length==4){
                        s.setLastName(parts[0]);
                        s.setFirstName(parts[1]);
                        s.setPsuId(parts[2]);
                        s.setTeam(parts[3]);
                        if(!(s.getTeam().compareTo(team)==0)){
                            students2.put(s.getPsuId(), s);
                        }
                    }// if
                }
            } catch (IOException e) {
                System.out.println("IO EXCEPTION");
            }
            in.close();
            System.out.println("SIZE: "+students2.size());
            Collection c = students2.values();
            Iterator itr = c.iterator();
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename,false)));
            while(itr.hasNext()){
                Student s = (Student)itr.next();
                System.out.println(s.getFirstName());
                 
                try {
                  out.write(String.format(
                          "%s!%s!%s!%s%n",
                          s.getLastName(),
                          s.getFirstName(),
                          s.getPsuId(),
                          s.getTeam()
                  ));
                } catch (FileNotFoundException ex) {
                    System.out.println("FILE NOT FOUND");
                } catch (IOException ex) {
                    System.out.println("IO EXCEPTION");
                }
            }
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Roster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String toXML() {
	String xml = null;
	try {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    XMLEncoder encoder = new XMLEncoder(out);
	    encoder.writeObject(students.values().toArray()); // serialize to XML
	    encoder.close();
	    xml = out.toString(); // stringify
	}
	catch(Exception e) { }
//        System.out.println(xml);
        //System.out.println(xml.trim());
	return xml;
    }
}