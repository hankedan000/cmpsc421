<%-- 
    Document   : browse
    Created on : Apr 4, 2016, 8:59:56 PM
    Author     : djh5533
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Predictions Web Services</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript">
    	window.onload = function(){
                //JSP compiler adds an empty line before the xml content.
                //we manually remove it.
	        var content = $.trim($('#predDIV').html());
                //now remove the div element that is no longer needed
                $('#predDIV').remove();
                // parse the xml
                content = $.parseXML(content);
                var list = $(content).find('object');
                // Sort the list
                switch($('#sortField').html().trim()){
                    case 'firstname':
                        list = sortByFirst(list,$('#sortType').html().trim()=='true');
                        break;
                    case 'lastname':
                        list = sortByLast(list,$('#sortType').html().trim()=='true');
                        break;
                    case 'id':
                        list = sortById(list,$('#sortType').html().trim()=='true');
                        break;
                    case 'team':
                        list = sortByTeam(list,$('#sortType').html().trim()=='true');
                        break;
                }// switch
                $('#sortField').remove();
                $('#sortType').remove();
                $('#sortingInfo').hide();
                
                //below is specific to the output of the JavaBeans XMLEncoder
                $(list)
                  // now we can play with each <object>
                  .each(function(index, element){
                    // query & store the string field
                    var field = $(element);
                    // get the values we want
                    var void0 = field.find('[property="firstName"]').text();
                    var void1 = field.find('[property="lastName"]').text();
                    var void2 = field.find('[property="psuId"]').text();
                    var void3 = field.find('[property="team"]').text();
                    // and append some html in the <ol> element
                    $("tbody")
                      .append('<tr><td class="col-md-1">'+void0+'</td><td class="col-md-1">'+void1+'</td><td class="col-md-1">'+void2+'</td><td>'+void3+'</td></tr>');
                });                
            };
            
            function sortByFirst(list, ascending){
                list.sort(function(a,b){
                    var aLast = $(a).find('[property="firstName"]').text();
                    var bLast = $(b).find('[property="firstName"]').text();
                    
                    return aLast.localeCompare(bLast)*(ascending?1:-1);
                });
                
                $('#sortingInfo').html('f'+(ascending?'a':'d'));
                
                return list;
            }
            
            function sortByLast(list, ascending){
                list.sort(function(a,b){
                    var aLast = $(a).find('[property="lastName"]').text();
                    var bLast = $(b).find('[property="lastName"]').text();
                    
                    return aLast.localeCompare(bLast)*(ascending?1:-1);
                });
                
                $('#sortingInfo').html('l'+(ascending?'a':'d'));
                
                return list;
            }
            
            function sortById(list, ascending){
                list.sort(function(a,b){
                    var aLast = $(a).find('[property="psuId"]').text();
                    var bLast = $(b).find('[property="psuId"]').text();
                    
                    return aLast.localeCompare(bLast)*(ascending?1:-1);
                });
                
                $('#sortingInfo').html('i'+(ascending?'a':'d'));
                
                return list;
            }
            
            function sortByTeam(list, ascending){
                list.sort(function(a,b){
                    var result;
                    var aVal = parseInt($(a).find('[property="team"]').text());
                    var bVal = parseInt($(b).find('[property="team"]').text());
                    
                    if(aVal<bVal)           result = -1;
                    else if(aVal==bVal)     result = 0;
                    else                    result = 1;

                    return result*(ascending?1:-1);
                });
                
                $('#sortingInfo').html('t'+(ascending?'a':'d'));
                
                return list;
            }
            </script>
    </head>
    <body>
        <div id="predDIV">
            <jsp:useBean id    = "roster" 
                         type  = "Models.Roster" 
                         class = "Models.Roster"> 
            </jsp:useBean>  
              <% 
                 System.out.println(request.getParameter("name"));
                 String verb = request.getMethod();

                 if (!verb.equalsIgnoreCase("GET")) {
                   response.sendError(response.SC_METHOD_NOT_ALLOWED,
                                      "GET requests only are allowed.");
                 }
                 // If it's a GET request, return the predictions.
                 else {
                   // Object reference application has the value 
                   // pageContext.getServletContext()
                   roster.setServletContext(application);
                   out.println(roster.getPredictions());
                 }
              %>
        </div>
        <div id="sortField"> 
              <% 
                 out.print(request.getParameter("field"));
              %>
        </div>
        <div id="sortType"> 
              <% 
                 out.print(request.getParameter("ascending"));
              %>
        </div>
        <a href="roster/browse">browse</a>
        <a href="roster/addForm">add</a>
        <a href="roster/deleteForm">delete</a>
        <a href="roster/deleteTeamForm">deleteTeam</a>
        <a href="roster/updateForm">update</a>
        <br/>
        <table class="table table-striped">
            <tr>
                <th><a href="#" id = "firstname">First Name</a></th>
                <th><a href="#" id = "lastname">Last Name</a></th>
                <th><a href="#" id = "id">Student ID</a></th>
                <th><a href="#" id = "team">Team Number</a></th>
            </tr>
        </table>
        <div id="sortingInfo"></div>
        
        <script>
            var firstascending = true;
            var lastascending = true;
            var idascending = true;
            var teamascending = true;
            $('#firstname').click(function(){
                if($('#sortingInfo').html()=='fa'){
                    window.location.href = "/RosterMVC/roster/browse?field=firstname&ascending=false";
                } else {
                    window.location.href = "/RosterMVC/roster/browse?field=firstname&ascending=true";
                }
            });

            $('#lastname').click(function(){
                if($('#sortingInfo').html()=='la'){
                    window.location.href = "/RosterMVC/roster/browse?field=lastname&ascending=false";
                } else {
                    window.location.href = "/RosterMVC/roster/browse?field=lastname&ascending=true";
                }
            });

            $('#id').click(function(){
                if($('#sortingInfo').html()=='ia'){
                    window.location.href = "/RosterMVC/roster/browse?field=id&ascending=false";
                } else {
                    window.location.href = "/RosterMVC/roster/browse?field=id&ascending=true";
                }
            });

            $('#team').click(function(){
                if($('#sortingInfo').html()=='ta'){
                    window.location.href = "/RosterMVC/roster/browse?field=team&ascending=false";
                } else {
                    window.location.href = "/RosterMVC/roster/browse?field=team&ascending=true";
                }
            });
        </script>
    </body>
</html>
