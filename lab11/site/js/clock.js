function startTime() {
	var today = new Date();
	var ampm = "am"
    	var h = today.getHours();
	if(h > 12){
		h = h - 12;
		ampm = "pm"
	}
	if(h == 12)
		ampm = "pm"
	if(h == 0)
		h = 12;
	var m = today.getMinutes();
	var s = today.getSeconds();
	m = addZero(m);
	s = addZero(s);
	document.getElementById('clock').innerHTML = h + ":" + m  + ":" + s + " " + ampm;
	var t = setTimeout(startTime, 500);
}

function addZero(i) {
	if (i < 10) {i = "0" + i};
	return i;
}
