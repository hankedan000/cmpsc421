var express = require('express');
var fs = require('fs');
var bodyParser  = require('body-parser');
var nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport();

var ChatServer  = require('./CloudChat/ChatServer');

//setup the root path
var root = __dirname;
ChatServer.gettool.root = root;
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

var correctAnswers = ["<script>", "fourtyfour", "yes", "onetwo", "slashslash","nineyay","messagebox","post", "get", "true"];
var userAnswers = new Array();
var users = {};
app.set("jsonp callback", true);

app.get('/', function (req, res) {
	fs.readFile('home.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});
app.get('/about.html', function (req, res) {
	fs.readFile('about.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});
app.get('/side.html', function (req, res) {
	fs.readFile('side.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});


app.get('/ThreeRegion/*', function (req, res) {
	var fileName = root +req.path;
	  res.sendFile(fileName, function (err) {
	    if (err) {
	      console.log(err);
	      res.status(err.status).end();
	    }
	    else {
	      console.log('Sent:', req.path);
	    }
	  });
});

app.get('/js/*', function (req, res) {
	var fileName = root+req.path;
	  res.sendFile(fileName, function (err) {
	    if (err) {
	      console.log(err);
	      res.status(err.status).end();
	    }
	    else {
	      console.log('Sent:', req.path);
	    }
	  });
});

app.get('/CloudChat/*', ChatServer.gettool);
app.get('/Syllabus/*', function (req, res) {
	var fileName = root+req.path;
	  res.sendFile(fileName, function (err) {
	    if (err) {
	      console.log(err);
	      res.status(err.status).end();
	    }
	    else {
	      console.log('Sent:', req.path);
	    }
	  });
});

var users = [];

app.get('/EvalTool/*', function (req, res) {
	var fileName = root + req.path;
	  res.sendFile(fileName, function (err) {
	    if (err) {
	      console.log(err);
	      res.status(err.status).end();
	    }
	    else {
	      console.log('Sent:', req.path);
	    }
	  });
});

app.post('/EvalTool/newuser', function (req, res) {
  console.log('REQBODY: ', req.body);
  if(req.body.user){
    var newUser = {};
    newUser.answers = []; // create empty list of answers
    users[req.body.user] = newUser;// add to database
    console.log('created new user '+req.body.user);
    res.send('created');
  } else {
    res.status(401);
  }
});

app.post('/EvalTool/next', function (req, res) {
  console.log('REQBODY1: ', req.body);
  var user = users[req.body.user];

  if (!user) {
    //TODO: Redirect user to beginning of quiz. They're not logged in
    console.log('Invalid user: ', user);
    return;
  }

  users[req.body.user].answers[req.body.questionnum - 1] = req.body.answer;
  res.sendStatus(200);

});

app.post('/EvalTool/submit', function (req, res) {
  users[req.body.user].answers[req.body.questionnum - 1] = req.body.answer;
  console.log(  users[req.body.user].answers[i]);
  count = 0;
  for (var i=0; i<10; i++){
    if(users[req.body.user].answers[i]==correctAnswers[i]){
      count++;
    }
  }
  console.log("Total Right " + count);
  users[req.body.user].score = count;
  res.json({
    correctAnswers: count
  });
});

app.post('/EvalTool/sendmail', function(req, res) {
  var user = users[req.body.user];
	var mymail = {};
  if(user){
    mymail['from']=req.body.fname+"<"+req.body.femail+">"; // sender address
  	mymail['to']=req.body.tname+"<"+req.body.temail+">"; // comma separated list of receivers
  	mymail['subject']=req.body.subject;
  	mymail['text']='Your score was '+user.score+'/10'; // plaintext body
  	smtpTransport.sendMail(mymail, function(error, info){
  	   if(error){
  		   console.log(error);
  	   }else{
  		   console.log("Message sent: " + info.response);
  	   }
  	});
  	var msg = '<p>The evalutation has been sent.</p>';
    msg += '<p>Thank you.</p>';
  	res.send(msg);
  } else {
    res.send('Failed to send message: no user provided');
  }// if
});
app.get('/home.html', function (req, res) {
	fs.readFile('home.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});
app.listen(8080, function() {
  console.log('Server running at http://127.0.0.1:8080/');
});
