var fs = require('fs');
var bodyParser  = require('body-parser');

var root = __dirname+'/';
console.log(root);

module.exports = function(router){
  router.route('/*').get(function (req, res) {
  	var fileName = root + req.path;
  	  res.sendFile(fileName, function (err) {
  	    if (err) {
  	      console.log(err);
  	      res.status(err.status).end();
  	    }
  	    else {
  	      console.log('Sent:', req.path);
  	    }
  	  });
  });
}
