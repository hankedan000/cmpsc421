var fs = require('fs');
var bodyParser  = require('body-parser');
var multer  = require('multer')
var upload  = multer({ dest: 'uploads/' });

var root = __dirname+'/';

var lectures = new Array();

module.exports = function(router){
  router.post('/save',function (req, res) {
    var newLecture = [];
    newLecture.slides = [];
    newLecture.slides[req.body.slideID] = {};
    newLecture.slides[req.body.slideID].title = req.body.title;
    console.log(req.body.backgroundColor);
    newLecture.slides[req.body.slideID].backgroundColor = req.body.backgroundColor;
    newLecture.slides[req.body.slideID].items = req.body.items;
    lectures[req.body.lectureID] = newLecture;
    console.log(lectures);
    res.sendStatus(200);
  });

  router.route('/restore').get(function (req, res) {
    console.log("sent lect: "+req.query.lectureID+" slide: "+req.query.slideID)
    console.log(lectures[req.query.lectureID].slides[req.query.slideID]);
    res.json(lectures[req.query.lectureID].slides[req.query.slideID]);
  });

  router.post('/upload', upload.any(), function (req, res) {
    res.send(req.files[0].filename);
  });

  router.route('/*').get(function (req, res) {
  	var fileName = root + req.path;
  	  res.sendFile(fileName, function (err) {
  	    if (err) {
  	      console.log(err);
  	      res.status(err.status).end();
  	    }
  	    else {
  	      console.log('Sent:', req.path);
  	    }
  	  });
  });
}
