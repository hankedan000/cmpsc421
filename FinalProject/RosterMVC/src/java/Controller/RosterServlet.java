package Controller;

import Models.Roster;
import Models.Student;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

/** Servlet that reads a customer ID and displays
 *  information on the account balance of the customer
 *  who has that ID.
 */

@WebServlet("/roster/*")
public class RosterServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws ServletException, IOException {
        System.out.printf("URL GET: %s",request.getPathInfo());
        String address;
        switch(request.getPathInfo()){
            case "/browse":
                address = "/WEB-INF/views/browse.jsp";
                break;
            case "/addForm":
                address = "/WEB-INF/views/addForm.jsp";
                break;
            case "/deleteForm":
                address = "/WEB-INF/views/deleteForm.jsp";
                break;
            case "/deleteTeamForm":
                address = "/WEB-INF/views/deleteTeamForm.jsp";
                break;
            case "/updateForm":
                address = "/WEB-INF/views/updateForm.jsp";
                break;
            default:
                address = request.getPathInfo();
                break;
        }// switch

        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.printf("URL POST: %s",request.getPathInfo());
        String fName, lName, id, team;
        Student s = new Student();
        
        switch(request.getPathInfo()){
            case "/roster/add":
                fName = request.getParameter("firstname");
                lName = request.getParameter("lastname");
                id = request.getParameter("psuId");
                team = request.getParameter("team");
                
                s.setFirstName(fName);
                s.setLastName(lName);
                s.setPsuId(id);
                s.setTeam(team);
                
                if(fName!=null && lName!=null && id!=null && team!=null){
                    Roster.add(s);
                }// if
                
                break;
            case "/roster/student":
                id = request.getParameter("psuId");
                Roster.delete(id);
                
                break;
            case "/roster/team":
                team = request.getParameter("team");
                Roster.deleteTeam(team);
                
                break;
            case "/roster/update":
                fName = request.getParameter("firstname");
                lName = request.getParameter("lastname");
                id = request.getParameter("psuId");
                team = request.getParameter("team");
                
                s.setFirstName(fName);
                s.setLastName(lName);
                s.setPsuId(id);
                s.setTeam(team);
                
                if(fName!=null && lName!=null && id!=null && team!=null){
                    Roster.update(s);
                }// if
                
                break;
        }
    }
}
