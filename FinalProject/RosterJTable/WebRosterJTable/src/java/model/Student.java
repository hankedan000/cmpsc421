package model;

public class Student {
    private String studentId;
    private String lastName;
    private String firstName;
    private String team;

    public String getStudentId() {
        return studentId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getTeam() {
        return team;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setFirstName(String first) {
        this.firstName = first;
    }

    public void setLastName(String last) {
        this.lastName = last;
    }

    public void setTeam(String team) {
        this.team = team;
    }
}