var fs = require('fs');
var bodyParser  = require('body-parser');
var nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport();

var root = __dirname+'/';
console.log(root);

module.exports = function(router){
  var users = [];

  router.route('/*').get(function (req, res) {
  	var fileName = root + req.path;
  	  res.sendFile(fileName, function (err) {
  	    if (err) {
  	      console.log(err);
  	      res.status(err.status).end();
  	    }
  	    else {
  	      console.log('Sent:', req.path);
  	    }
  	  });
  });

  router.route('/newuser').post(function (req, res) {
    console.log('REQBODY: ', req.user);
    if(req.body.user){
      var newUser = {};
      newUser.answers = []; // create empty list of answers
      users[req.body.user] = newUser;// add to database
      console.log('created new user '+req.body.user);
      res.send('created');
    } else {
      res.status(401);
    }
  });

  router.route('/next').post(function (req, res) {
    console.log('REQBODY1: ', req.body);
    var user = users[req.body.user];

    if (!user) {
      //TODO: Redirect user to beginning of quiz. They're not logged in
      console.log('Invalid user: ', user);
      return;
    }

    users[req.body.user].answers[req.body.questionnum - 1] = req.body.answer;
    res.sendStatus(200);

  });

  router.post('/submit', function (req, res) {
    users[req.body.user].answers[req.body.questionnum - 1] = req.body.answer;
    count = 0;
    for (var i=0; i<10; i++){
      if(users[req.body.user].answers[i]==correctAnswers[i]){
        count++;
      }
    }
    console.log("Total Right " + count);
    users[req.body.user].score = count;
    res.json({
      correctAnswers: count
    });
  });

  router.post('/sendmail', function(req, res) {
    var user = users[req.body.user];
  	var mymail = {};
    if(user){
      mymail['from']=req.body.fname+"<"+req.body.femail+">"; // sender address
    	mymail['to']=req.body.tname+"<"+req.body.temail+">"; // comma separated list of receivers
    	mymail['subject']=req.body.subject;
    	mymail['text']='Your score was '+user.score+'/10'; // plaintext body
    	smtpTransport.sendMail(mymail, function(error, info){
    	   if(error){
    		   console.log(error);
    	   }else{
    		   console.log("Message sent: " + info.response);
    	   }
    	});
    	var msg = '<p>The evalutation has been sent.</p>';
      msg += '<p>Thank you.</p>';
    	res.send(msg);
    } else {
      res.send('Failed to send message: no user provided');
    }// if
  });
}
