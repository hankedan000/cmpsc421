<!DOCTYPE html>
<html>
<head><title>Update User</title>
<link href="./css/styles.css" 
      rel="stylesheet" type="text/css"/>
<head>
<body>
<div align="center">
    <a href="roster/browse">browse</a>
    <a href="roster/addForm">add</a>
    <a href="roster/deleteForm">delete</a>
    <a href="roster/deleteTeamForm">deleteTeam</a>
    <a href="roster/updateForm">update</a>
    <br/>
      <form action="roster/update" method="POST">
        Enter the PSU ID of the user to edit: 
        <input type="text" name="psuId"/><br/>
       Enter information to be changed. Any blank fields will remain the same<br/><br/>
          First Name: 
        <input type="text" name="firstname"/><br/>
          Last Name: 
        <input type="text" name="lastname"/><br/>
          Team
        <input type="text" name="team"/><br/>
        <input type="submit" value="Update User"/>
      </form>
</div></body></html>