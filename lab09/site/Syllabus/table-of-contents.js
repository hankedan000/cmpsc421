//Add this to body of HTML file.
// <script src="table-of-contents.js"></script>
// <script>
// window.onload = function() {
//   loadTableOfContents();
// }
// </script>

function loadTableOfContents(){
  var headers = document.getElementsByTagName('h4');
  var tableOfContents = "";
  for (var i =0; i<headers.length; i++){
    headers[i].id = "header-" + i;
    tableOfContents += ('<a href="#header-'+ i +'"">' + headers[i].innerHTML + '</a>' + '</br>');
  }
  var node = document.createElement("div");
  node.id = "tableOfContents";
  node.innerHTML = tableOfContents;
  document.body.insertBefore(node, document.body.firstChild);
}
