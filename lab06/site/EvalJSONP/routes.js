var fs = require('fs');
var bodyParser  = require('body-parser');
var nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport();

var root = __dirname+'/';
console.log(root);

module.exports = function(router){
  var users = {};
  var correctAnswers = ["<script>", "fourtyfour", "yes", "onetwo", "slashslash","nineyay","messagebox","post", "get", "true"];

  router.get('/newuser', function (req, res) {
    var id = req.query.id;
    if(id){
      var newUser = {};
      newUser.name = req.query.name;
      newUser.answers = [];// create empty list of answers
      users[id] = newUser;// add to database
      console.log('created new user!\n'+JSON.stringify(newUser));
      res.jsonp('created');
    } else {
      res.status(401);
    }
  });

  router.get('/next', function (req, res) {
    var id = req.query.id;
    var num = parseInt(req.query.num);
    var userAnswers = users[id].answers;

    userAnswers[num] = req.query.answer;

    var result = {answer:userAnswers[num+1]};
    console.log("NEXT");
    console.log("QUERY\t"+JSON.stringify(req.query));
    console.log("RESULT\t"+JSON.stringify(result));
    res.jsonp(result);
  });

  router.get('/last', function(req, res){
    var id = req.query.id;
    var num = parseInt(req.query.num);
    var userAnswers = users[id].answers;

    userAnswers[num] = req.query.answer;

    var result = {answer:userAnswers[num-1]};
    console.log("LAST");
    console.log("QUERY\t"+JSON.stringify(req.query));
    console.log("RESULT\t"+JSON.stringify(result));
    res.jsonp(result);
  });

  router.get('/getAnswer', function (req, res){
    console.log("GETTING ANSWERS");
    var id = req.query.id;
    console.log(users[id]);
    var userAnswers = users[id].answers;
    var result = {answer: userAnswers[req.query.num]};
    res.jsonp(result);
  });

  router.post('/submit', function (req, res) {
    users[req.body.id].answers[req.body.num] = req.body.answer;
    count = 0;
    for (var i=0; i<10; i++){
      if(users[req.body.id].answers[i+1]==correctAnswers[i]){
        count++;
      }
    }
    console.log("Total Right " + count);
    users[req.body.id].score = count;
    res.json({
      correctAnswers: count
    });
  });

  router.post('/sendmail', function(req, res) {
    var user = users[req.body.id];
  	var mymail = {};
    if(user){
      mymail['from']=req.body.fname+"<"+req.body.femail+">"; // sender address
    	mymail['to']=req.body.tname+"<"+req.body.temail+">"; // comma separated list of receivers
    	mymail['subject']=req.body.subject;
    	mymail['text']='Your score was '+user.score+'/10'; // plaintext body
    	smtpTransport.sendMail(mymail, function(error, info){
    	   if(error){
    		   console.log(error);
    	   }else{
    		   console.log("Message sent: " + info.response);
    	   }
    	});
    	var msg = '<p>The evalutation has been sent.</p>';
      msg += '<p>Thank you.</p>';
    	res.json({message:msg});
    } else {
      res.json({message:'Failed to send message: no user provided'});
    }// if
  });

  router.route('/*').get(function (req, res) {
  	var fileName = root + req.path;
  	  res.sendFile(fileName, function (err) {
  	    if (err) {
  	      console.log(err);
  	      res.status(err.status).end();
  	    }
  	    else {
  	      console.log('Sent:', req.path);
  	    }
  	  });
  });
}
