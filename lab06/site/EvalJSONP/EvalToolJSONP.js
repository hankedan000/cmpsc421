function getJSONP(url, handler) {
  //create a unique callback for this request
  var cbnum = "cb" +getJSONP.counter++;
  var cbname = "getJSONP." +cbnum;
  //Add the callback to the url query using form encoding
  if(url.indexOf("?") === -1)
    url += "?callback =" + cbname;
  else
    url += "&callback=" +cbname;
  //create the script element that will send this request
  var script = document.createElement("script");
  //script.type = "application/json";
  //Define the allback function that will be invoked by the script
  getJSONP[cbnum] = function(response) {
      try {
        handler(response);
      }
      finally {
        delete getJSONP[cbnum];
        script.parentNode.removeChild(script);
      }
  };

  script.src = url;
  document.body.appendChild(script);
}
getJSONP.counter = 0; //A counter we use to createunique callback name
