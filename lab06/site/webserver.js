var express = require('express');
var fs = require('fs');
var bodyParser  = require('body-parser');
var ChatServer  = require('./CloudChat/ChatServer');

//setup the root path
var root = __dirname;
ChatServer.gettool.root = root;
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.set('jsonp callback',true);

// ADD ROUTES
var evalRouter = express.Router();
//require('./EvalTool/routes.js')(router);
require('./EvalJSONP/routes.js')(evalRouter);
app.use('/EvalTool', evalRouter);
var schedulerouter = express.Router();
require('./Schedule/routes.js')(schedulerouter);
app.use('/Schedule', schedulerouter);

var correctAnswers = ["<script>", "fourtyfour", "yes", "onetwo", "slashslash","nineyay","messagebox","post", "get", "true"];
var userAnswers = new Array();
var users = {};

app.get('/', function (req, res) {
	fs.readFile('home.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});
app.get('/about.html', function (req, res) {
	fs.readFile('about.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});
app.get('/side.html', function (req, res) {
	fs.readFile('side.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});


app.get('/ThreeRegion/*', function (req, res) {
	var fileName = root +req.path;
	  res.sendFile(fileName, function (err) {
	    if (err) {
	      console.log(err);
	      res.status(err.status).end();
	    }
	    else {
	      console.log('Sent:', req.path);
	    }
	  });
});

app.get('/js/*', function (req, res) {
	var fileName = root+req.path;
	  res.sendFile(fileName, function (err) {
	    if (err) {
	      console.log(err);
	      res.status(err.status).end();
	    }
	    else {
	      console.log('Sent:', req.path);
	    }
	  });
});

app.get('/CloudChat/*', ChatServer.gettool);
app.get('/Syllabus/*', function (req, res) {
	var fileName = root+req.path;
	  res.sendFile(fileName, function (err) {
	    if (err) {
	      console.log(err);
	      res.status(err.status).end();
	    }
	    else {
	      console.log('Sent:', req.path);
	    }
	  });
});

app.get('/home.html', function (req, res) {
	fs.readFile('home.html', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
	res.send(data);
	});
});
app.listen(8080, function() {
  console.log('Server running at http://127.0.0.1:8080/');
});
